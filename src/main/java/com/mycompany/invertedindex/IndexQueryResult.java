/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.invertedindex;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author imasson
 */
public class IndexQueryResult {

    private final ArrayList<Integer> result;
    private final String[] data;

    /**
     * 
     * @param document The documents obtained from the query as ArrayList<integer>
     * 
     * @param data The original text as String[] used to return the contents of each document
     * 
     */
    IndexQueryResult(ArrayList<Integer> document, String[] data) {
        this.result = document;
        this.data = data;
    }

    public int found() {
        return result.size();
    }

    /**
     * 
     * @return 
     */
    public String[] getResultArray() {
        ArrayList<String> list = new ArrayList<String>();
        for (Iterator<Integer> it = result.iterator(); it.hasNext();) {
            list.add(data[it.next()]);
        }
        String[] resul_array = new String[list.size()];
        return list.toArray(resul_array);
    }

    public void print() {
        for (Iterator<Integer> it = result.iterator(); it.hasNext();) {
            System.out.println(data[it.next()]);

        }
    }
}
