/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.invertedindex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ivan
 */
public class InvertedIndex {

    private HashMap<String, ArrayList<Integer>> index = new HashMap<String, ArrayList<Integer>>();

    private final String[] originalData;

    /**
     * 
     * @param array The entry data to be indexed as String[]
     */
    InvertedIndex(String[] array) {
        this.originalData = array;
        for (int i = 0; i < array.length; i++) {
            String[] position = array[i].split("\\s|\\W");//the strings are split by a regexp using spaces and metachars
            for (String it : position) {
                if (!it.equals("")) {
                    putIndexValue(it.toLowerCase(), i);//adds the respective document for each string to the inverted index
                }
            }
        }
        // printIndex();
    }

    /**
     * 
     * @param query String given as entry to index search.
     * 
     * @return Returns a IndexQueryResult object.
     *
     */
    public IndexQueryResult get(String query) {
        IndexQueryResult result = new IndexQueryResult(index.get(query), originalData);
        return result;
    }

    private void putIndexValue(String value, int i) {
        ArrayList<Integer> oldArray = index.get(value);
        if (oldArray == null) {
            oldArray = new ArrayList<Integer>();
        }

        if (!oldArray.contains(i)) {
            oldArray.add(i);
            index.put(value, oldArray);
        }
    }

    private void printIndex() {
        for (Map.Entry<String, ArrayList<Integer>> entry : index.entrySet()) {
            String string = entry.getKey();
            System.out.println("***clave: " + string);
            ArrayList<Integer> is = entry.getValue();
            System.out.println("***valor: " + is.toString());
        }
    }

}
