
package com.mycompany.invertedindex;
import java.util.Arrays;
import java.util.Hashtable;
/**
 * Hello world!
 *
 */
public class Mainclass {

    static private String[] data = new String[]{
        "A brilliant, festive study of JS Bach uses literature and painting to illuminate his 'dance-impregnated' music, writes Peter Conrad",
        "Fatima Bhutto on Malala Yousafzai's fearless and still-controversial memoir",
        "Grisham's sequel to A Time to Kill is a solid courtroom drama about racial prejudice marred by a flawless white hero, writes John O'Connell",
        "This strange repackaging of bits and pieces does the Man Booker winner no favours, says Sam Leith",
        "Another book with music related content"
    };

    public static void main(String[] args) {
        InvertedIndex i = new InvertedIndex(data);
        /*first query*/
        IndexQueryResult r= i.get("writes");
        System.out.println(Arrays.toString(r.getResultArray()));
        System.out.println("Search key word found in: "+r.found()+" documents");
        
        /*second query*/
        IndexQueryResult r2= i.get("and");
        System.out.println(Arrays.toString(r2.getResultArray()));
        System.out.println("Search key word found in: "+r2.found()+" documents");
        
        
        
    }
}
